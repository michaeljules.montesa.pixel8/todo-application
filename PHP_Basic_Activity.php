<?php
header('Content-Type: text/plain');
class API {

    function printFullName($name){
        echo "Full name: $name \r\n";
    }

    function printHobbies($hobbies){
        foreach ($hobbies as $hobby){
            echo "$hobby\r\n";
        }

    }

    function personalInfo($details){
        echo "Age: {$details['age']}\n";
        echo "Email: {$details['email']}\n";
        echo "Birthday: {$details['birthday']}\n";
    }
}



$person = new API();
$person->printFullName("Michael Jules Montesa");

echo "Hobbies: \r\n", $person->printHobbies(array("Playing Video Games", "Listening to Music", "Playing Music"));

$person->personalInfo(array("age"=>22, "email"=>"michaeljules.montesa.pixel8@gmail.com", "birthday"=>"January 23, 2001"));

?>