<html>

<?php
include ('connection.php');

$name = $email = $age = '';

try{
    if(isset($_POST['searchButton'])){
        $idNumber = $_POST['ID'];
        $searchQuery = mysqli_query($connection, "SELECT * FROM information WHERE ID=$idNumber");
        while($row = mysqli_fetch_assoc($searchQuery)){
            $name = $row['NAME'];
            $email = $row['EMAIL'];
            $age = $row['AGE'];
           }
    }
} catch (Exception $e){
    echo "Search Error: $e";
}


try{
    if (isset($_POST['updateButton'])){

        $name = $_POST['fullName'];
        $email = $_POST['email'];
        $age = $_POST['age'];
        $idNumber = $_POST['id'];
    
        mysqli_query($connection, "UPDATE information 
                                    SET NAME='$name', 
                                        EMAIL='$email', 
                                        AGE='$age' 
                                        WHERE information.ID='$idNumber'");
        
    }
} catch (Exception $e){
    echo "Update Error: $e";
}

try{
    if (isset($_POST['deleteButton'])){
        $idNumber = $_POST['id'];
        mysqli_query($connection, "DELETE FROM information WHERE ID=$idNumber");
    }
} catch (Exception $e){
    echo "Delete Error: $e";
}



?>
<header>
    <h1>Update Data</h1>
</header>
<body>
    <fieldset>
        <legend>Update Record</legend>
            <form method="POST">
                <p><label>Enter ID number to edit: <input type="number" name="ID" value=""/></label></p>
                <p><input type="submit" name="searchButton" value="Search"></p>
            </form>
            <form method="POST">
                <p><input type="hidden" name="id" value="<?php echo $idNumber?>"></p>
                <p><label>Full name: <input type="text" name="fullName" placeholder="Full Name" value="<?php echo $name?>"/></label></p>
                <p><label>Email: <input type="text" name="email" placeholder="Email" value="<?php echo $email?>"/></label></p>
                <p><label>Age: <input type="text" name="age" placeholder="Age" value="<?php echo $age?>"/></label></p>
                <p><input type="submit" name="updateButton" value="Update"></p>
                <p><input type="submit" name="deleteButton" value="Delete"></p>
            </form>
            <form action="index.php">
                <input type="submit" value="Back to Home">
            </form>
            
            
    </fieldset>
    
</body>

</html>

