<!DOCTYPE html>
<html>
<?php

include ("connection.php");

class SQL{
    function createTable($connection){
        $sqlCommand = "CREATE TABLE information (ID INT PRIMARY KEY, 
                                                  NAME VARCHAR(255), 
                                                  EMAIL VARCHAR(255), 
                                                  AGE INT)";
        return mysqli_query($connection, $sqlCommand);
    }

    function insertData($connection){
        $fullName = $_POST['fullName'];
        $email = $_POST['email'];
        $age = $_POST['age'];
        $createQuery = "INSERT INTO `information`(`ID`, `NAME`, `EMAIL`, `AGE`) 
                        VALUES ('','$fullName','$email','$age')";
        mysqli_query($connection, $createQuery);
    }

    function readData($connection){
        $readDataQuery = mysqli_query($connection, "SELECT * FROM information");
        while ($row = mysqli_fetch_assoc($readDataQuery)){
            echo "ID: ", $row['ID'], "<br>";
            echo "Name: ", $row['NAME'], "<br>";
            echo "Email: ", $row['EMAIL'], "<br>";
            echo "Age: ", $row['AGE'], "<br><br>";
        }
    }
}

$main = new SQL();
// $main->createTable($connection);
if(isset($_POST['insertButton'])){ $main->insertData($connection); }
if(isset($_POST['viewButton'])){ $main->readData($connection); }
    
?>

    <header>
        <h1>PHP & MYSQL | Create Simple CRUD</h1>
    </header>
    <body>
        <form method="POST">
            <fieldset>
                <legend>Student Information</legend>
                <p><label>Full Name: <input type="text" name="fullName" value=""/></label></p>
                <p><label>Email Address: <input type="email" name="email" value=""/></label></p>
                <p><label>Age: <input type="number" name="age" value=""/></label></p>
            </fieldset>
            <input type = "submit" name = "insertButton" value = "Insert Data"><br><br>
            <input type = "submit" name = "viewButton" value = "View Data"><br><br>
        </form>
        <form action="update.php">
            <input type = "submit" name = "updateButton" value = "Update Data">
        </form>
    </body>

</html>